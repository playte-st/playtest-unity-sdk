﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// System.Text.StringBuilder
struct StringBuilder_t3822575854;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Text_StringBuilder3822575854.h"

// System.String Playtest.TinyJson.JSONWriter::ToJson(System.Object)
extern "C"  String_t* JSONWriter_ToJson_m2333493868 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Playtest.TinyJson.JSONWriter::AppendValue(System.Text.StringBuilder,System.Object)
extern "C"  void JSONWriter_AppendValue_m3277447425 (Il2CppObject * __this /* static, unused */, StringBuilder_t3822575854 * ___stringBuilder0, Il2CppObject * ___item1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
