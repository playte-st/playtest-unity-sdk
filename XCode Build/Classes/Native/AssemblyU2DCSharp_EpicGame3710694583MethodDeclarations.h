﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EpicGame
struct EpicGame_t3710694583;

#include "codegen/il2cpp-codegen.h"

// System.Void EpicGame::.ctor()
extern "C"  void EpicGame__ctor_m2162059204 (EpicGame_t3710694583 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EpicGame::OnGUI()
extern "C"  void EpicGame_OnGUI_m1657457854 (EpicGame_t3710694583 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
