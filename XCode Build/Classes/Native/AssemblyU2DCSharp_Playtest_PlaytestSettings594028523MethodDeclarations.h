﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Playtest.PlaytestSettings
struct PlaytestSettings_t594028523;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Playtest_PlaytestSettings_Feedba2008949566.h"

// System.Void Playtest.PlaytestSettings::.ctor()
extern "C"  void PlaytestSettings__ctor_m3159541082 (PlaytestSettings_t594028523 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Playtest.PlaytestSettings Playtest.PlaytestSettings::get_Current()
extern "C"  PlaytestSettings_t594028523 * PlaytestSettings_get_Current_m12268554 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Playtest.PlaytestSettings::get_Enabled()
extern "C"  bool PlaytestSettings_get_Enabled_m132293956 (PlaytestSettings_t594028523 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Playtest.PlaytestSettings::get_FeedbackId()
extern "C"  String_t* PlaytestSettings_get_FeedbackId_m2198327600 (PlaytestSettings_t594028523 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Playtest.PlaytestSettings::get_ApiKey()
extern "C"  String_t* PlaytestSettings_get_ApiKey_m3122106837 (PlaytestSettings_t594028523 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Playtest.PlaytestSettings/FeedbackGestureType Playtest.PlaytestSettings::get_FeedbackGesture()
extern "C"  int32_t PlaytestSettings_get_FeedbackGesture_m2497253894 (PlaytestSettings_t594028523 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Playtest.PlaytestSettings::get_InitialMessageCenterVisibility()
extern "C"  bool PlaytestSettings_get_InitialMessageCenterVisibility_m1070493321 (PlaytestSettings_t594028523 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Playtest.PlaytestSettings::get_NeedsValidaton()
extern "C"  bool PlaytestSettings_get_NeedsValidaton_m3919466096 (PlaytestSettings_t594028523 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
