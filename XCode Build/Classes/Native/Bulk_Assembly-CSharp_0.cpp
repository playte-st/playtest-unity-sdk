﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// EpicGame
struct EpicGame_t3710694583;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t2474804324;
// Playtest.PlaytestSettings
struct PlaytestSettings_t594028523;
// System.Object
struct Il2CppObject;
// System.Text.StringBuilder
struct StringBuilder_t3822575854;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "AssemblyU2DCSharp_U3CModuleU3E86524790.h"
#include "AssemblyU2DCSharp_U3CModuleU3E86524790MethodDeclarations.h"
#include "AssemblyU2DCSharp_EpicGame3710694583.h"
#include "AssemblyU2DCSharp_EpicGame3710694583MethodDeclarations.h"
#include "mscorlib_System_Void2779279689.h"
#include "UnityEngine_UnityEngine_MonoBehaviour3012272455MethodDeclarations.h"
#include "mscorlib_System_Boolean211005341.h"
#include "UnityEngine_UnityEngine_Vector33525329789MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion1891715979MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Matrix4x4277289660MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUI1522956648MethodDeclarations.h"
#include "AssemblyU2DCSharp_Playtest_PlaytestSDK1966770230MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUILayout2490032242MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2474804324MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Time1525492538MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Random3963434288MethodDeclarations.h"
#include "mscorlib_System_Single958209021MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2474804324.h"
#include "mscorlib_System_Single958209021.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"
#include "UnityEngine_UnityEngine_Quaternion1891715979.h"
#include "UnityEngine_UnityEngine_Matrix4x4277289660.h"
#include "mscorlib_System_String968488902.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_GUILayoutOption3151226183.h"
#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharp_Playtest_PlaytestSDK1966770230.h"
#include "UnityEngine_UnityEngine_Application450040189MethodDeclarations.h"
#include "AssemblyU2DCSharp_Playtest_PlaytestSettings594028523MethodDeclarations.h"
#include "AssemblyU2DCSharp_Playtest_PlaytestSettings594028523.h"
#include "AssemblyU2DCSharp_Playtest_PlaytestSettings_Feedba2008949566.h"
#include "mscorlib_System_Int322847414787.h"
#include "AssemblyU2DCSharp_Playtest_TinyJson_JSONWriter1588801931MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ScriptableObject184905905MethodDeclarations.h"
#include "mscorlib_System_String968488902MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object3878351788MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Resources1543782994MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object3878351788.h"
#include "AssemblyU2DCSharp_Playtest_PlaytestSettings_Feedba2008949566MethodDeclarations.h"
#include "AssemblyU2DCSharp_Playtest_TinyJson_JSONWriter1588801931.h"
#include "mscorlib_System_Text_StringBuilder3822575854MethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilder3822575854.h"
#include "mscorlib_System_Object837106420MethodDeclarations.h"
#include "mscorlib_System_Type2779229935MethodDeclarations.h"
#include "mscorlib_System_Type2779229935.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Reflection_FieldInfo1164929782.h"
#include "mscorlib_System_Reflection_PropertyInfo1490548369.h"
#include "mscorlib_System_RuntimeTypeHandle1864875887.h"
#include "mscorlib_System_Char2778706699.h"
#include "mscorlib_System_Reflection_FieldInfo1164929782MethodDeclarations.h"
#include "mscorlib_System_Reflection_MemberInfo2843033814MethodDeclarations.h"
#include "mscorlib_System_Reflection_MemberInfo2843033814.h"
#include "mscorlib_System_Reflection_PropertyInfo1490548369MethodDeclarations.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void EpicGame::.ctor()
extern "C"  void EpicGame__ctor_m2162059204 (EpicGame_t3710694583 * __this, const MethodInfo* method)
{
	{
		__this->set_isVisible_2((bool)1);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EpicGame::OnGUI()
extern Il2CppClass* GUI_t1522956648_il2cpp_TypeInfo_var;
extern Il2CppClass* GUILayoutOptionU5BU5D_t1890718142_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t2474804324_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t958209021_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m4186171898_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1602921772;
extern Il2CppCodeGenString* _stringLiteral2602914252;
extern Il2CppCodeGenString* _stringLiteral1357359189;
extern Il2CppCodeGenString* _stringLiteral2169052986;
extern Il2CppCodeGenString* _stringLiteral2606829;
extern Il2CppCodeGenString* _stringLiteral2440548579;
extern const uint32_t EpicGame_OnGUI_m1657457854_MetadataUsageId;
extern "C"  void EpicGame_OnGUI_m1657457854 (EpicGame_t3710694583 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EpicGame_OnGUI_m1657457854_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Dictionary_2_t2474804324 * V_1 = NULL;
	float V_2 = 0.0f;
	{
		Vector3_t3525329789  L_0 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t1891715979  L_1 = Quaternion_get_identity_m1743882806(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3525329789  L_2 = Vector3_get_one_m886467710(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3525329789  L_3 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_2, (5.0f), /*hidden argument*/NULL);
		Matrix4x4_t277289660  L_4 = Matrix4x4_TRS_m3596398659(NULL /*static, unused*/, L_0, L_1, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1522956648_il2cpp_TypeInfo_var);
		GUI_set_matrix_m1023514902(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		bool L_5 = PlaytestSDK_get_IsEnabled_m679325735(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUI_set_enabled_m262604887(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		bool L_6 = GUILayout_Button_m6468109(NULL /*static, unused*/, _stringLiteral1602921772, ((GUILayoutOptionU5BU5D_t1890718142*)SZArrayNew(GUILayoutOptionU5BU5D_t1890718142_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0059;
		}
	}
	{
		bool L_7 = __this->get_isVisible_2();
		int32_t L_8 = ((((int32_t)L_7) == ((int32_t)0))? 1 : 0);
		V_0 = (bool)L_8;
		__this->set_isVisible_2((bool)L_8);
		bool L_9 = V_0;
		PlaytestSDK_SetEngagementViewVisible_m2551168888(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
	}

IL_0059:
	{
		bool L_10 = GUILayout_Button_m6468109(NULL /*static, unused*/, _stringLiteral2602914252, ((GUILayoutOptionU5BU5D_t1890718142*)SZArrayNew(GUILayoutOptionU5BU5D_t1890718142_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0073;
		}
	}
	{
		PlaytestSDK_PresentFeedbackScreen_m601086905(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0073:
	{
		bool L_11 = GUILayout_Button_m6468109(NULL /*static, unused*/, _stringLiteral1357359189, ((GUILayoutOptionU5BU5D_t1890718142*)SZArrayNew(GUILayoutOptionU5BU5D_t1890718142_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_00c7;
		}
	}
	{
		Dictionary_2_t2474804324 * L_12 = (Dictionary_2_t2474804324 *)il2cpp_codegen_object_new(Dictionary_2_t2474804324_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m4186171898(L_12, /*hidden argument*/Dictionary_2__ctor_m4186171898_MethodInfo_var);
		V_1 = L_12;
		Dictionary_2_t2474804324 * L_13 = V_1;
		float L_14 = Time_get_time_m342192902(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_15 = L_14;
		Il2CppObject * L_16 = Box(Single_t958209021_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_13);
		VirtActionInvoker2< String_t*, Il2CppObject * >::Invoke(26 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Object>::Add(!0,!1) */, L_13, _stringLiteral2606829, L_16);
		Dictionary_2_t2474804324 * L_17 = V_1;
		float L_18 = Random_get_value_m2402066692(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = L_18;
		String_t* L_19 = Single_ToString_m5736032((&V_2), /*hidden argument*/NULL);
		NullCheck(L_17);
		VirtActionInvoker2< String_t*, Il2CppObject * >::Invoke(26 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Object>::Add(!0,!1) */, L_17, _stringLiteral2440548579, L_19);
		Dictionary_2_t2474804324 * L_20 = V_1;
		PlaytestSDK_MarkCheckpoint_m375979168(NULL /*static, unused*/, _stringLiteral2169052986, (bool)1, L_20, /*hidden argument*/NULL);
	}

IL_00c7:
	{
		return;
	}
}
// System.Void Playtest.PlaytestSDK::initializeSDK()
extern "C"  void PlaytestSDK_initializeSDK_m2163149047 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		bool L_0 = PlaytestSDK_get_IsEnabled_m679325735(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0070;
		}
	}
	{
		bool L_1 = Application_get_isPlaying_m987993960(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0070;
		}
	}
	{
		PlaytestSettings_t594028523 * L_2 = PlaytestSettings_get_Current_m12268554(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		String_t* L_3 = PlaytestSettings_get_FeedbackId_m2198327600(L_2, /*hidden argument*/NULL);
		PlaytestSettings_t594028523 * L_4 = PlaytestSettings_get_Current_m12268554(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		String_t* L_5 = PlaytestSettings_get_ApiKey_m3122106837(L_4, /*hidden argument*/NULL);
		PlaytestSDK_setupFeedbackId_m1420043508(NULL /*static, unused*/, L_3, L_5, /*hidden argument*/NULL);
		PlaytestSettings_t594028523 * L_6 = PlaytestSettings_get_Current_m12268554(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		bool L_7 = PlaytestSettings_get_NeedsValidaton_m3919466096(L_6, /*hidden argument*/NULL);
		PlaytestSDK_setNeedsValidation_m4058317696(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		PlaytestSettings_t594028523 * L_8 = PlaytestSettings_get_Current_m12268554(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		int32_t L_9 = PlaytestSettings_get_FeedbackGesture_m2497253894(L_8, /*hidden argument*/NULL);
		if ((((int32_t)L_9) == ((int32_t)3)))
		{
			goto IL_005b;
		}
	}
	{
		PlaytestSettings_t594028523 * L_10 = PlaytestSettings_get_Current_m12268554(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_10);
		int32_t L_11 = PlaytestSettings_get_FeedbackGesture_m2497253894(L_10, /*hidden argument*/NULL);
		PlaytestSDK_setFeedbackMethod_m697689533(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
	}

IL_005b:
	{
		PlaytestSettings_t594028523 * L_12 = PlaytestSettings_get_Current_m12268554(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_12);
		bool L_13 = PlaytestSettings_get_InitialMessageCenterVisibility_m1070493321(L_12, /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_0070;
		}
	}
	{
		PlaytestSDK_SetEngagementViewVisible_m2551168888(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
	}

IL_0070:
	{
		return;
	}
}
// System.Boolean Playtest.PlaytestSDK::get_IsEnabled()
extern "C"  bool PlaytestSDK_get_IsEnabled_m679325735 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		PlaytestSettings_t594028523 * L_0 = PlaytestSettings_get_Current_m12268554(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = PlaytestSettings_get_Enabled_m132293956(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void Playtest.PlaytestSDK::_setupFeedbackAndAppId(System.String,System.String)
extern "C" void DEFAULT_CALL _setupFeedbackAndAppId(char*, char*);
extern "C"  void PlaytestSDK__setupFeedbackAndAppId_m1012030785 (Il2CppObject * __this /* static, unused */, String_t* ___feedbackId0, String_t* ___apiKey1, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, char*);

	// Marshaling of parameter '___feedbackId0' to native representation
	char* ____feedbackId0_marshaled = NULL;
	____feedbackId0_marshaled = il2cpp_codegen_marshal_string(___feedbackId0);

	// Marshaling of parameter '___apiKey1' to native representation
	char* ____apiKey1_marshaled = NULL;
	____apiKey1_marshaled = il2cpp_codegen_marshal_string(___apiKey1);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_setupFeedbackAndAppId)(____feedbackId0_marshaled, ____apiKey1_marshaled);

	// Marshaling cleanup of parameter '___feedbackId0' native representation
	il2cpp_codegen_marshal_free(____feedbackId0_marshaled);
	____feedbackId0_marshaled = NULL;

	// Marshaling cleanup of parameter '___apiKey1' native representation
	il2cpp_codegen_marshal_free(____apiKey1_marshaled);
	____apiKey1_marshaled = NULL;

}
// System.Void Playtest.PlaytestSDK::_setNeedsValidation(System.Boolean)
extern "C" void DEFAULT_CALL _setNeedsValidation(int32_t);
extern "C"  void PlaytestSDK__setNeedsValidation_m2477860215 (Il2CppObject * __this /* static, unused */, bool ___needsValidaton0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Marshaling of parameter '___needsValidaton0' to native representation

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_setNeedsValidation)(___needsValidaton0);

	// Marshaling cleanup of parameter '___needsValidaton0' native representation

}
// System.Void Playtest.PlaytestSDK::_setFeedbackMethod(System.Int32)
extern "C" void DEFAULT_CALL _setFeedbackMethod(int32_t);
extern "C"  void PlaytestSDK__setFeedbackMethod_m1896283631 (Il2CppObject * __this /* static, unused */, int32_t ___feedbackMethod0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Marshaling of parameter '___feedbackMethod0' to native representation

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_setFeedbackMethod)(___feedbackMethod0);

	// Marshaling cleanup of parameter '___feedbackMethod0' native representation

}
// System.Void Playtest.PlaytestSDK::_setEngagementViewHidden(System.Boolean)
extern "C" void DEFAULT_CALL _setEngagementViewHidden(int32_t);
extern "C"  void PlaytestSDK__setEngagementViewHidden_m3390981853 (Il2CppObject * __this /* static, unused */, bool ___hidden0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Marshaling of parameter '___hidden0' to native representation

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_setEngagementViewHidden)(___hidden0);

	// Marshaling cleanup of parameter '___hidden0' native representation

}
// System.Void Playtest.PlaytestSDK::_presentFeedbackScreen()
extern "C" void DEFAULT_CALL _presentFeedbackScreen();
extern "C"  void PlaytestSDK__presentFeedbackScreen_m211580482 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_presentFeedbackScreen)();

}
// System.Void Playtest.PlaytestSDK::_markCheckpoint(System.String,System.Boolean,System.String)
extern "C" void DEFAULT_CALL _markCheckpoint(char*, int32_t, char*);
extern "C"  void PlaytestSDK__markCheckpoint_m382453560 (Il2CppObject * __this /* static, unused */, String_t* ___id0, bool ___withScreenshot1, String_t* ___eventData2, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, int32_t, char*);

	// Marshaling of parameter '___id0' to native representation
	char* ____id0_marshaled = NULL;
	____id0_marshaled = il2cpp_codegen_marshal_string(___id0);

	// Marshaling of parameter '___withScreenshot1' to native representation

	// Marshaling of parameter '___eventData2' to native representation
	char* ____eventData2_marshaled = NULL;
	____eventData2_marshaled = il2cpp_codegen_marshal_string(___eventData2);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_markCheckpoint)(____id0_marshaled, ___withScreenshot1, ____eventData2_marshaled);

	// Marshaling cleanup of parameter '___id0' native representation
	il2cpp_codegen_marshal_free(____id0_marshaled);
	____id0_marshaled = NULL;

	// Marshaling cleanup of parameter '___withScreenshot1' native representation

	// Marshaling cleanup of parameter '___eventData2' native representation
	il2cpp_codegen_marshal_free(____eventData2_marshaled);
	____eventData2_marshaled = NULL;

}
// System.Void Playtest.PlaytestSDK::setupFeedbackId(System.String,System.String)
extern "C"  void PlaytestSDK_setupFeedbackId_m1420043508 (Il2CppObject * __this /* static, unused */, String_t* ___feedbackId0, String_t* ___apiKey1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___feedbackId0;
		String_t* L_1 = ___apiKey1;
		PlaytestSDK__setupFeedbackAndAppId_m1012030785(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Playtest.PlaytestSDK::setNeedsValidation(System.Boolean)
extern "C"  void PlaytestSDK_setNeedsValidation_m4058317696 (Il2CppObject * __this /* static, unused */, bool ___needsValidation0, const MethodInfo* method)
{
	{
		bool L_0 = ___needsValidation0;
		PlaytestSDK__setNeedsValidation_m2477860215(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Playtest.PlaytestSDK::setFeedbackMethod(Playtest.PlaytestSettings/FeedbackGestureType)
extern "C"  void PlaytestSDK_setFeedbackMethod_m697689533 (Il2CppObject * __this /* static, unused */, int32_t ___gestureType0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___gestureType0;
		PlaytestSDK__setFeedbackMethod_m1896283631(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Playtest.PlaytestSDK::SetEngagementViewVisible(System.Boolean)
extern "C"  void PlaytestSDK_SetEngagementViewVisible_m2551168888 (Il2CppObject * __this /* static, unused */, bool ___visible0, const MethodInfo* method)
{
	{
		bool L_0 = ___visible0;
		PlaytestSDK__setEngagementViewHidden_m3390981853(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Playtest.PlaytestSDK::PresentFeedbackScreen()
extern "C"  void PlaytestSDK_PresentFeedbackScreen_m601086905 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		PlaytestSDK__presentFeedbackScreen_m211580482(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Playtest.PlaytestSDK::MarkCheckpoint(System.String,System.Boolean,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void PlaytestSDK_MarkCheckpoint_m375979168 (Il2CppObject * __this /* static, unused */, String_t* ___id0, bool ___withScreenshot1, Dictionary_2_t2474804324 * ___args2, const MethodInfo* method)
{
	{
		String_t* L_0 = ___id0;
		bool L_1 = ___withScreenshot1;
		Dictionary_2_t2474804324 * L_2 = ___args2;
		String_t* L_3 = JSONWriter_ToJson_m2333493868(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		PlaytestSDK__markCheckpoint_m382453560(NULL /*static, unused*/, L_0, L_1, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Playtest.PlaytestSettings::.ctor()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t PlaytestSettings__ctor_m3159541082_MetadataUsageId;
extern "C"  void PlaytestSettings__ctor_m3159541082 (PlaytestSettings_t594028523 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlaytestSettings__ctor_m3159541082_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_initialMessageCenterVisibility_5((bool)1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_android_feedbackId_7(L_0);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_android_apiKey_8(L_1);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_ios_feedbackId_9(L_2);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_ios_apiKey_10(L_3);
		__this->set_feedbackGesture_11(3);
		ScriptableObject__ctor_m1827087273(__this, /*hidden argument*/NULL);
		return;
	}
}
// Playtest.PlaytestSettings Playtest.PlaytestSettings::get_Current()
extern Il2CppClass* PlaytestSettings_t594028523_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral926176297;
extern const uint32_t PlaytestSettings_get_Current_m12268554_MetadataUsageId;
extern "C"  PlaytestSettings_t594028523 * PlaytestSettings_get_Current_m12268554 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlaytestSettings_get_Current_m12268554_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		PlaytestSettings_t594028523 * L_0 = ((PlaytestSettings_t594028523_StaticFields*)PlaytestSettings_t594028523_il2cpp_TypeInfo_var->static_fields)->get__current_2();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0024;
		}
	}
	{
		Object_t3878351788 * L_2 = Resources_Load_m2187391845(NULL /*static, unused*/, _stringLiteral926176297, /*hidden argument*/NULL);
		((PlaytestSettings_t594028523_StaticFields*)PlaytestSettings_t594028523_il2cpp_TypeInfo_var->static_fields)->set__current_2(((PlaytestSettings_t594028523 *)IsInstClass(L_2, PlaytestSettings_t594028523_il2cpp_TypeInfo_var)));
	}

IL_0024:
	{
		PlaytestSettings_t594028523 * L_3 = ((PlaytestSettings_t594028523_StaticFields*)PlaytestSettings_t594028523_il2cpp_TypeInfo_var->static_fields)->get__current_2();
		return L_3;
	}
}
// System.Boolean Playtest.PlaytestSettings::get_Enabled()
extern "C"  bool PlaytestSettings_get_Enabled_m132293956 (PlaytestSettings_t594028523 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_ios_enabled_4();
		return L_0;
	}
}
// System.String Playtest.PlaytestSettings::get_FeedbackId()
extern "C"  String_t* PlaytestSettings_get_FeedbackId_m2198327600 (PlaytestSettings_t594028523 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_ios_feedbackId_9();
		return L_0;
	}
}
// System.String Playtest.PlaytestSettings::get_ApiKey()
extern "C"  String_t* PlaytestSettings_get_ApiKey_m3122106837 (PlaytestSettings_t594028523 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_ios_apiKey_10();
		return L_0;
	}
}
// Playtest.PlaytestSettings/FeedbackGestureType Playtest.PlaytestSettings::get_FeedbackGesture()
extern "C"  int32_t PlaytestSettings_get_FeedbackGesture_m2497253894 (PlaytestSettings_t594028523 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_feedbackGesture_11();
		return L_0;
	}
}
// System.Boolean Playtest.PlaytestSettings::get_InitialMessageCenterVisibility()
extern "C"  bool PlaytestSettings_get_InitialMessageCenterVisibility_m1070493321 (PlaytestSettings_t594028523 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_initialMessageCenterVisibility_5();
		return L_0;
	}
}
// System.Boolean Playtest.PlaytestSettings::get_NeedsValidaton()
extern "C"  bool PlaytestSettings_get_NeedsValidaton_m3919466096 (PlaytestSettings_t594028523 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_needsValidation_6();
		return L_0;
	}
}
// System.String Playtest.TinyJson.JSONWriter::ToJson(System.Object)
extern Il2CppClass* StringBuilder_t3822575854_il2cpp_TypeInfo_var;
extern const uint32_t JSONWriter_ToJson_m2333493868_MetadataUsageId;
extern "C"  String_t* JSONWriter_ToJson_m2333493868 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONWriter_ToJson_m2333493868_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t3822575854 * V_0 = NULL;
	{
		StringBuilder_t3822575854 * L_0 = (StringBuilder_t3822575854 *)il2cpp_codegen_object_new(StringBuilder_t3822575854_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m135953004(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		StringBuilder_t3822575854 * L_1 = V_0;
		Il2CppObject * L_2 = ___item0;
		JSONWriter_AppendValue_m3277447425(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_3 = V_0;
		NullCheck(L_3);
		String_t* L_4 = StringBuilder_ToString_m350379841(L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void Playtest.TinyJson.JSONWriter::AppendValue(System.Text.StringBuilder,System.Object)
extern const Il2CppType* String_t_0_0_0_var;
extern const Il2CppType* Int32_t2847414787_0_0_0_var;
extern const Il2CppType* Single_t958209021_0_0_0_var;
extern const Il2CppType* Double_t534516614_0_0_0_var;
extern const Il2CppType* Boolean_t211005341_0_0_0_var;
extern const Il2CppType* Dictionary_2_t2776849293_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t211005341_il2cpp_TypeInfo_var;
extern Il2CppClass* IList_t1612618265_il2cpp_TypeInfo_var;
extern Il2CppClass* ICollection_t3761522009_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_t1654916945_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_t287189635_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3392903;
extern Il2CppCodeGenString* _stringLiteral3569038;
extern Il2CppCodeGenString* _stringLiteral97196323;
extern Il2CppCodeGenString* _stringLiteral3938;
extern Il2CppCodeGenString* _stringLiteral1112;
extern const uint32_t JSONWriter_AppendValue_m3277447425_MetadataUsageId;
extern "C"  void JSONWriter_AppendValue_m3277447425 (Il2CppObject * __this /* static, unused */, StringBuilder_t3822575854 * ___stringBuilder0, Il2CppObject * ___item1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONWriter_AppendValue_m3277447425_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	bool V_1 = false;
	Il2CppObject * V_2 = NULL;
	int32_t V_3 = 0;
	Type_t * V_4 = NULL;
	Il2CppObject * V_5 = NULL;
	bool V_6 = false;
	Il2CppObject * V_7 = NULL;
	Il2CppObject * V_8 = NULL;
	bool V_9 = false;
	FieldInfoU5BU5D_t1144794227* V_10 = NULL;
	int32_t V_11 = 0;
	Il2CppObject * V_12 = NULL;
	PropertyInfoU5BU5D_t1348579340* V_13 = NULL;
	int32_t V_14 = 0;
	Il2CppObject * V_15 = NULL;
	Il2CppObject * V_16 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	StringBuilder_t3822575854 * G_B11_0 = NULL;
	StringBuilder_t3822575854 * G_B10_0 = NULL;
	String_t* G_B12_0 = NULL;
	StringBuilder_t3822575854 * G_B12_1 = NULL;
	{
		Il2CppObject * L_0 = ___item1;
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		StringBuilder_t3822575854 * L_1 = ___stringBuilder0;
		NullCheck(L_1);
		StringBuilder_Append_m3898090075(L_1, _stringLiteral3392903, /*hidden argument*/NULL);
		return;
	}

IL_0013:
	{
		Il2CppObject * L_2 = ___item1;
		NullCheck(L_2);
		Type_t * L_3 = Object_GetType_m2022236990(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Type_t * L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5))))
		{
			goto IL_004e;
		}
	}
	{
		StringBuilder_t3822575854 * L_6 = ___stringBuilder0;
		NullCheck(L_6);
		StringBuilder_Append_m2143093878(L_6, ((int32_t)34), /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_7 = ___stringBuilder0;
		Il2CppObject * L_8 = ___item1;
		NullCheck(L_7);
		StringBuilder_Append_m3898090075(L_7, ((String_t*)CastclassSealed(L_8, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_9 = ___stringBuilder0;
		NullCheck(L_9);
		StringBuilder_Append_m2143093878(L_9, ((int32_t)34), /*hidden argument*/NULL);
		goto IL_034c;
	}

IL_004e:
	{
		Type_t * L_10 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_11 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Int32_t2847414787_0_0_0_var), /*hidden argument*/NULL);
		if ((((Il2CppObject*)(Type_t *)L_10) == ((Il2CppObject*)(Type_t *)L_11)))
		{
			goto IL_007e;
		}
	}
	{
		Type_t * L_12 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_13 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Single_t958209021_0_0_0_var), /*hidden argument*/NULL);
		if ((((Il2CppObject*)(Type_t *)L_12) == ((Il2CppObject*)(Type_t *)L_13)))
		{
			goto IL_007e;
		}
	}
	{
		Type_t * L_14 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_15 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Double_t534516614_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_14) == ((Il2CppObject*)(Type_t *)L_15))))
		{
			goto IL_0090;
		}
	}

IL_007e:
	{
		StringBuilder_t3822575854 * L_16 = ___stringBuilder0;
		Il2CppObject * L_17 = ___item1;
		NullCheck(L_17);
		String_t* L_18 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_17);
		NullCheck(L_16);
		StringBuilder_Append_m3898090075(L_16, L_18, /*hidden argument*/NULL);
		goto IL_034c;
	}

IL_0090:
	{
		Type_t * L_19 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_20 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Boolean_t211005341_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_19) == ((Il2CppObject*)(Type_t *)L_20))))
		{
			goto IL_00c6;
		}
	}
	{
		StringBuilder_t3822575854 * L_21 = ___stringBuilder0;
		Il2CppObject * L_22 = ___item1;
		G_B10_0 = L_21;
		if (!((*(bool*)((bool*)UnBox (L_22, Boolean_t211005341_il2cpp_TypeInfo_var)))))
		{
			G_B11_0 = L_21;
			goto IL_00b6;
		}
	}
	{
		G_B12_0 = _stringLiteral3569038;
		G_B12_1 = G_B10_0;
		goto IL_00bb;
	}

IL_00b6:
	{
		G_B12_0 = _stringLiteral97196323;
		G_B12_1 = G_B11_0;
	}

IL_00bb:
	{
		NullCheck(G_B12_1);
		StringBuilder_Append_m3898090075(G_B12_1, G_B12_0, /*hidden argument*/NULL);
		goto IL_034c;
	}

IL_00c6:
	{
		Il2CppObject * L_23 = ___item1;
		if (!((Il2CppObject *)IsInst(L_23, IList_t1612618265_il2cpp_TypeInfo_var)))
		{
			goto IL_012b;
		}
	}
	{
		StringBuilder_t3822575854 * L_24 = ___stringBuilder0;
		NullCheck(L_24);
		StringBuilder_Append_m2143093878(L_24, ((int32_t)91), /*hidden argument*/NULL);
		V_1 = (bool)1;
		Il2CppObject * L_25 = ___item1;
		V_2 = ((Il2CppObject *)IsInst(L_25, IList_t1612618265_il2cpp_TypeInfo_var));
		V_3 = 0;
		goto IL_0111;
	}

IL_00ea:
	{
		bool L_26 = V_1;
		if (!L_26)
		{
			goto IL_00f7;
		}
	}
	{
		V_1 = (bool)0;
		goto IL_0100;
	}

IL_00f7:
	{
		StringBuilder_t3822575854 * L_27 = ___stringBuilder0;
		NullCheck(L_27);
		StringBuilder_Append_m2143093878(L_27, ((int32_t)44), /*hidden argument*/NULL);
	}

IL_0100:
	{
		StringBuilder_t3822575854 * L_28 = ___stringBuilder0;
		Il2CppObject * L_29 = V_2;
		int32_t L_30 = V_3;
		NullCheck(L_29);
		Il2CppObject * L_31 = InterfaceFuncInvoker1< Il2CppObject *, int32_t >::Invoke(0 /* System.Object System.Collections.IList::get_Item(System.Int32) */, IList_t1612618265_il2cpp_TypeInfo_var, L_29, L_30);
		JSONWriter_AppendValue_m3277447425(NULL /*static, unused*/, L_28, L_31, /*hidden argument*/NULL);
		int32_t L_32 = V_3;
		V_3 = ((int32_t)((int32_t)L_32+(int32_t)1));
	}

IL_0111:
	{
		int32_t L_33 = V_3;
		Il2CppObject * L_34 = V_2;
		NullCheck(L_34);
		int32_t L_35 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.ICollection::get_Count() */, ICollection_t3761522009_il2cpp_TypeInfo_var, L_34);
		if ((((int32_t)L_33) < ((int32_t)L_35)))
		{
			goto IL_00ea;
		}
	}
	{
		StringBuilder_t3822575854 * L_36 = ___stringBuilder0;
		NullCheck(L_36);
		StringBuilder_Append_m2143093878(L_36, ((int32_t)93), /*hidden argument*/NULL);
		goto IL_034c;
	}

IL_012b:
	{
		Type_t * L_37 = V_0;
		NullCheck(L_37);
		bool L_38 = VirtFuncInvoker0< bool >::Invoke(79 /* System.Boolean System.Type::get_IsGenericType() */, L_37);
		if (!L_38)
		{
			goto IL_0222;
		}
	}
	{
		Type_t * L_39 = V_0;
		NullCheck(L_39);
		Type_t * L_40 = VirtFuncInvoker0< Type_t * >::Invoke(78 /* System.Type System.Type::GetGenericTypeDefinition() */, L_39);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_41 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Dictionary_2_t2776849293_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_40) == ((Il2CppObject*)(Type_t *)L_41))))
		{
			goto IL_0222;
		}
	}
	{
		Type_t * L_42 = V_0;
		NullCheck(L_42);
		TypeU5BU5D_t3431720054* L_43 = VirtFuncInvoker0< TypeU5BU5D_t3431720054* >::Invoke(75 /* System.Type[] System.Type::GetGenericArguments() */, L_42);
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, 0);
		int32_t L_44 = 0;
		V_4 = ((L_43)->GetAt(static_cast<il2cpp_array_size_t>(L_44)));
		Type_t * L_45 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_46 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		if ((((Il2CppObject*)(Type_t *)L_45) == ((Il2CppObject*)(Type_t *)L_46)))
		{
			goto IL_0173;
		}
	}
	{
		StringBuilder_t3822575854 * L_47 = ___stringBuilder0;
		NullCheck(L_47);
		StringBuilder_Append_m3898090075(L_47, _stringLiteral3938, /*hidden argument*/NULL);
		return;
	}

IL_0173:
	{
		StringBuilder_t3822575854 * L_48 = ___stringBuilder0;
		NullCheck(L_48);
		StringBuilder_Append_m2143093878(L_48, ((int32_t)123), /*hidden argument*/NULL);
		Il2CppObject * L_49 = ___item1;
		V_5 = ((Il2CppObject *)IsInst(L_49, IDictionary_t1654916945_il2cpp_TypeInfo_var));
		V_6 = (bool)1;
		Il2CppObject * L_50 = V_5;
		NullCheck(L_50);
		Il2CppObject * L_51 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Collections.ICollection System.Collections.IDictionary::get_Keys() */, IDictionary_t1654916945_il2cpp_TypeInfo_var, L_50);
		NullCheck(L_51);
		Il2CppObject * L_52 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t287189635_il2cpp_TypeInfo_var, L_51);
		V_8 = L_52;
	}

IL_0195:
	try
	{ // begin try (depth: 1)
		{
			goto IL_01ed;
		}

IL_019a:
		{
			Il2CppObject * L_53 = V_8;
			NullCheck(L_53);
			Il2CppObject * L_54 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_53);
			V_7 = L_54;
			bool L_55 = V_6;
			if (!L_55)
			{
				goto IL_01b2;
			}
		}

IL_01aa:
		{
			V_6 = (bool)0;
			goto IL_01bb;
		}

IL_01b2:
		{
			StringBuilder_t3822575854 * L_56 = ___stringBuilder0;
			NullCheck(L_56);
			StringBuilder_Append_m2143093878(L_56, ((int32_t)44), /*hidden argument*/NULL);
		}

IL_01bb:
		{
			StringBuilder_t3822575854 * L_57 = ___stringBuilder0;
			NullCheck(L_57);
			StringBuilder_Append_m2143093878(L_57, ((int32_t)34), /*hidden argument*/NULL);
			StringBuilder_t3822575854 * L_58 = ___stringBuilder0;
			Il2CppObject * L_59 = V_7;
			NullCheck(L_58);
			StringBuilder_Append_m3898090075(L_58, ((String_t*)CastclassSealed(L_59, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
			StringBuilder_t3822575854 * L_60 = ___stringBuilder0;
			NullCheck(L_60);
			StringBuilder_Append_m3898090075(L_60, _stringLiteral1112, /*hidden argument*/NULL);
			StringBuilder_t3822575854 * L_61 = ___stringBuilder0;
			Il2CppObject * L_62 = V_5;
			Il2CppObject * L_63 = V_7;
			NullCheck(L_62);
			Il2CppObject * L_64 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t1654916945_il2cpp_TypeInfo_var, L_62, L_63);
			JSONWriter_AppendValue_m3277447425(NULL /*static, unused*/, L_61, L_64, /*hidden argument*/NULL);
		}

IL_01ed:
		{
			Il2CppObject * L_65 = V_8;
			NullCheck(L_65);
			bool L_66 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_65);
			if (L_66)
			{
				goto IL_019a;
			}
		}

IL_01f9:
		{
			IL2CPP_LEAVE(0x214, FINALLY_01fe);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_01fe;
	}

FINALLY_01fe:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_67 = V_8;
			V_16 = ((Il2CppObject *)IsInst(L_67, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			Il2CppObject * L_68 = V_16;
			if (L_68)
			{
				goto IL_020c;
			}
		}

IL_020b:
		{
			IL2CPP_END_FINALLY(510)
		}

IL_020c:
		{
			Il2CppObject * L_69 = V_16;
			NullCheck(L_69);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_69);
			IL2CPP_END_FINALLY(510)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(510)
	{
		IL2CPP_JUMP_TBL(0x214, IL_0214)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0214:
	{
		StringBuilder_t3822575854 * L_70 = ___stringBuilder0;
		NullCheck(L_70);
		StringBuilder_Append_m2143093878(L_70, ((int32_t)125), /*hidden argument*/NULL);
		goto IL_034c;
	}

IL_0222:
	{
		StringBuilder_t3822575854 * L_71 = ___stringBuilder0;
		NullCheck(L_71);
		StringBuilder_Append_m2143093878(L_71, ((int32_t)123), /*hidden argument*/NULL);
		V_9 = (bool)1;
		Type_t * L_72 = V_0;
		NullCheck(L_72);
		FieldInfoU5BU5D_t1144794227* L_73 = VirtFuncInvoker0< FieldInfoU5BU5D_t1144794227* >::Invoke(45 /* System.Reflection.FieldInfo[] System.Type::GetFields() */, L_72);
		V_10 = L_73;
		V_11 = 0;
		goto IL_02ad;
	}

IL_023e:
	{
		FieldInfoU5BU5D_t1144794227* L_74 = V_10;
		int32_t L_75 = V_11;
		NullCheck(L_74);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_74, L_75);
		int32_t L_76 = L_75;
		NullCheck(((L_74)->GetAt(static_cast<il2cpp_array_size_t>(L_76))));
		bool L_77 = VirtFuncInvoker0< bool >::Invoke(20 /* System.Boolean System.Reflection.FieldInfo::get_IsPublic() */, ((L_74)->GetAt(static_cast<il2cpp_array_size_t>(L_76))));
		if (!L_77)
		{
			goto IL_02a7;
		}
	}
	{
		FieldInfoU5BU5D_t1144794227* L_78 = V_10;
		int32_t L_79 = V_11;
		NullCheck(L_78);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_78, L_79);
		int32_t L_80 = L_79;
		Il2CppObject * L_81 = ___item1;
		NullCheck(((L_78)->GetAt(static_cast<il2cpp_array_size_t>(L_80))));
		Il2CppObject * L_82 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(17 /* System.Object System.Reflection.FieldInfo::GetValue(System.Object) */, ((L_78)->GetAt(static_cast<il2cpp_array_size_t>(L_80))), L_81);
		V_12 = L_82;
		Il2CppObject * L_83 = V_12;
		if (!L_83)
		{
			goto IL_02a7;
		}
	}
	{
		bool L_84 = V_9;
		if (!L_84)
		{
			goto IL_0270;
		}
	}
	{
		V_9 = (bool)0;
		goto IL_0279;
	}

IL_0270:
	{
		StringBuilder_t3822575854 * L_85 = ___stringBuilder0;
		NullCheck(L_85);
		StringBuilder_Append_m2143093878(L_85, ((int32_t)44), /*hidden argument*/NULL);
	}

IL_0279:
	{
		StringBuilder_t3822575854 * L_86 = ___stringBuilder0;
		NullCheck(L_86);
		StringBuilder_Append_m2143093878(L_86, ((int32_t)34), /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_87 = ___stringBuilder0;
		FieldInfoU5BU5D_t1144794227* L_88 = V_10;
		int32_t L_89 = V_11;
		NullCheck(L_88);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_88, L_89);
		int32_t L_90 = L_89;
		NullCheck(((L_88)->GetAt(static_cast<il2cpp_array_size_t>(L_90))));
		String_t* L_91 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, ((L_88)->GetAt(static_cast<il2cpp_array_size_t>(L_90))));
		NullCheck(L_87);
		StringBuilder_Append_m3898090075(L_87, L_91, /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_92 = ___stringBuilder0;
		NullCheck(L_92);
		StringBuilder_Append_m3898090075(L_92, _stringLiteral1112, /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_93 = ___stringBuilder0;
		Il2CppObject * L_94 = V_12;
		JSONWriter_AppendValue_m3277447425(NULL /*static, unused*/, L_93, L_94, /*hidden argument*/NULL);
	}

IL_02a7:
	{
		int32_t L_95 = V_11;
		V_11 = ((int32_t)((int32_t)L_95+(int32_t)1));
	}

IL_02ad:
	{
		int32_t L_96 = V_11;
		FieldInfoU5BU5D_t1144794227* L_97 = V_10;
		NullCheck(L_97);
		if ((((int32_t)L_96) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_97)->max_length)))))))
		{
			goto IL_023e;
		}
	}
	{
		Type_t * L_98 = V_0;
		NullCheck(L_98);
		PropertyInfoU5BU5D_t1348579340* L_99 = VirtFuncInvoker0< PropertyInfoU5BU5D_t1348579340* >::Invoke(53 /* System.Reflection.PropertyInfo[] System.Type::GetProperties() */, L_98);
		V_13 = L_99;
		V_14 = 0;
		goto IL_0338;
	}

IL_02c8:
	{
		PropertyInfoU5BU5D_t1348579340* L_100 = V_13;
		int32_t L_101 = V_14;
		NullCheck(L_100);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_100, L_101);
		int32_t L_102 = L_101;
		NullCheck(((L_100)->GetAt(static_cast<il2cpp_array_size_t>(L_102))));
		bool L_103 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Reflection.PropertyInfo::get_CanRead() */, ((L_100)->GetAt(static_cast<il2cpp_array_size_t>(L_102))));
		if (!L_103)
		{
			goto IL_0332;
		}
	}
	{
		PropertyInfoU5BU5D_t1348579340* L_104 = V_13;
		int32_t L_105 = V_14;
		NullCheck(L_104);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_104, L_105);
		int32_t L_106 = L_105;
		Il2CppObject * L_107 = ___item1;
		NullCheck(((L_104)->GetAt(static_cast<il2cpp_array_size_t>(L_106))));
		Il2CppObject * L_108 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject *, ObjectU5BU5D_t11523773* >::Invoke(22 /* System.Object System.Reflection.PropertyInfo::GetValue(System.Object,System.Object[]) */, ((L_104)->GetAt(static_cast<il2cpp_array_size_t>(L_106))), L_107, (ObjectU5BU5D_t11523773*)(ObjectU5BU5D_t11523773*)NULL);
		V_15 = L_108;
		Il2CppObject * L_109 = V_15;
		if (!L_109)
		{
			goto IL_0332;
		}
	}
	{
		bool L_110 = V_9;
		if (!L_110)
		{
			goto IL_02fb;
		}
	}
	{
		V_9 = (bool)0;
		goto IL_0304;
	}

IL_02fb:
	{
		StringBuilder_t3822575854 * L_111 = ___stringBuilder0;
		NullCheck(L_111);
		StringBuilder_Append_m2143093878(L_111, ((int32_t)44), /*hidden argument*/NULL);
	}

IL_0304:
	{
		StringBuilder_t3822575854 * L_112 = ___stringBuilder0;
		NullCheck(L_112);
		StringBuilder_Append_m2143093878(L_112, ((int32_t)34), /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_113 = ___stringBuilder0;
		PropertyInfoU5BU5D_t1348579340* L_114 = V_13;
		int32_t L_115 = V_14;
		NullCheck(L_114);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_114, L_115);
		int32_t L_116 = L_115;
		NullCheck(((L_114)->GetAt(static_cast<il2cpp_array_size_t>(L_116))));
		String_t* L_117 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, ((L_114)->GetAt(static_cast<il2cpp_array_size_t>(L_116))));
		NullCheck(L_113);
		StringBuilder_Append_m3898090075(L_113, L_117, /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_118 = ___stringBuilder0;
		NullCheck(L_118);
		StringBuilder_Append_m3898090075(L_118, _stringLiteral1112, /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_119 = ___stringBuilder0;
		Il2CppObject * L_120 = V_15;
		JSONWriter_AppendValue_m3277447425(NULL /*static, unused*/, L_119, L_120, /*hidden argument*/NULL);
	}

IL_0332:
	{
		int32_t L_121 = V_14;
		V_14 = ((int32_t)((int32_t)L_121+(int32_t)1));
	}

IL_0338:
	{
		int32_t L_122 = V_14;
		PropertyInfoU5BU5D_t1348579340* L_123 = V_13;
		NullCheck(L_123);
		if ((((int32_t)L_122) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_123)->max_length)))))))
		{
			goto IL_02c8;
		}
	}
	{
		StringBuilder_t3822575854 * L_124 = ___stringBuilder0;
		NullCheck(L_124);
		StringBuilder_Append_m2143093878(L_124, ((int32_t)125), /*hidden argument*/NULL);
	}

IL_034c:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
