﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Playtest.PlaytestSettings
struct PlaytestSettings_t594028523;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_ScriptableObject184905905.h"
#include "AssemblyU2DCSharp_Playtest_PlaytestSettings_Feedba2008949566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Playtest.PlaytestSettings
struct  PlaytestSettings_t594028523  : public ScriptableObject_t184905905
{
public:
	// System.Boolean Playtest.PlaytestSettings::android_enabled
	bool ___android_enabled_3;
	// System.Boolean Playtest.PlaytestSettings::ios_enabled
	bool ___ios_enabled_4;
	// System.Boolean Playtest.PlaytestSettings::initialMessageCenterVisibility
	bool ___initialMessageCenterVisibility_5;
	// System.Boolean Playtest.PlaytestSettings::needsValidation
	bool ___needsValidation_6;
	// System.String Playtest.PlaytestSettings::android_feedbackId
	String_t* ___android_feedbackId_7;
	// System.String Playtest.PlaytestSettings::android_apiKey
	String_t* ___android_apiKey_8;
	// System.String Playtest.PlaytestSettings::ios_feedbackId
	String_t* ___ios_feedbackId_9;
	// System.String Playtest.PlaytestSettings::ios_apiKey
	String_t* ___ios_apiKey_10;
	// Playtest.PlaytestSettings/FeedbackGestureType Playtest.PlaytestSettings::feedbackGesture
	int32_t ___feedbackGesture_11;

public:
	inline static int32_t get_offset_of_android_enabled_3() { return static_cast<int32_t>(offsetof(PlaytestSettings_t594028523, ___android_enabled_3)); }
	inline bool get_android_enabled_3() const { return ___android_enabled_3; }
	inline bool* get_address_of_android_enabled_3() { return &___android_enabled_3; }
	inline void set_android_enabled_3(bool value)
	{
		___android_enabled_3 = value;
	}

	inline static int32_t get_offset_of_ios_enabled_4() { return static_cast<int32_t>(offsetof(PlaytestSettings_t594028523, ___ios_enabled_4)); }
	inline bool get_ios_enabled_4() const { return ___ios_enabled_4; }
	inline bool* get_address_of_ios_enabled_4() { return &___ios_enabled_4; }
	inline void set_ios_enabled_4(bool value)
	{
		___ios_enabled_4 = value;
	}

	inline static int32_t get_offset_of_initialMessageCenterVisibility_5() { return static_cast<int32_t>(offsetof(PlaytestSettings_t594028523, ___initialMessageCenterVisibility_5)); }
	inline bool get_initialMessageCenterVisibility_5() const { return ___initialMessageCenterVisibility_5; }
	inline bool* get_address_of_initialMessageCenterVisibility_5() { return &___initialMessageCenterVisibility_5; }
	inline void set_initialMessageCenterVisibility_5(bool value)
	{
		___initialMessageCenterVisibility_5 = value;
	}

	inline static int32_t get_offset_of_needsValidation_6() { return static_cast<int32_t>(offsetof(PlaytestSettings_t594028523, ___needsValidation_6)); }
	inline bool get_needsValidation_6() const { return ___needsValidation_6; }
	inline bool* get_address_of_needsValidation_6() { return &___needsValidation_6; }
	inline void set_needsValidation_6(bool value)
	{
		___needsValidation_6 = value;
	}

	inline static int32_t get_offset_of_android_feedbackId_7() { return static_cast<int32_t>(offsetof(PlaytestSettings_t594028523, ___android_feedbackId_7)); }
	inline String_t* get_android_feedbackId_7() const { return ___android_feedbackId_7; }
	inline String_t** get_address_of_android_feedbackId_7() { return &___android_feedbackId_7; }
	inline void set_android_feedbackId_7(String_t* value)
	{
		___android_feedbackId_7 = value;
		Il2CppCodeGenWriteBarrier(&___android_feedbackId_7, value);
	}

	inline static int32_t get_offset_of_android_apiKey_8() { return static_cast<int32_t>(offsetof(PlaytestSettings_t594028523, ___android_apiKey_8)); }
	inline String_t* get_android_apiKey_8() const { return ___android_apiKey_8; }
	inline String_t** get_address_of_android_apiKey_8() { return &___android_apiKey_8; }
	inline void set_android_apiKey_8(String_t* value)
	{
		___android_apiKey_8 = value;
		Il2CppCodeGenWriteBarrier(&___android_apiKey_8, value);
	}

	inline static int32_t get_offset_of_ios_feedbackId_9() { return static_cast<int32_t>(offsetof(PlaytestSettings_t594028523, ___ios_feedbackId_9)); }
	inline String_t* get_ios_feedbackId_9() const { return ___ios_feedbackId_9; }
	inline String_t** get_address_of_ios_feedbackId_9() { return &___ios_feedbackId_9; }
	inline void set_ios_feedbackId_9(String_t* value)
	{
		___ios_feedbackId_9 = value;
		Il2CppCodeGenWriteBarrier(&___ios_feedbackId_9, value);
	}

	inline static int32_t get_offset_of_ios_apiKey_10() { return static_cast<int32_t>(offsetof(PlaytestSettings_t594028523, ___ios_apiKey_10)); }
	inline String_t* get_ios_apiKey_10() const { return ___ios_apiKey_10; }
	inline String_t** get_address_of_ios_apiKey_10() { return &___ios_apiKey_10; }
	inline void set_ios_apiKey_10(String_t* value)
	{
		___ios_apiKey_10 = value;
		Il2CppCodeGenWriteBarrier(&___ios_apiKey_10, value);
	}

	inline static int32_t get_offset_of_feedbackGesture_11() { return static_cast<int32_t>(offsetof(PlaytestSettings_t594028523, ___feedbackGesture_11)); }
	inline int32_t get_feedbackGesture_11() const { return ___feedbackGesture_11; }
	inline int32_t* get_address_of_feedbackGesture_11() { return &___feedbackGesture_11; }
	inline void set_feedbackGesture_11(int32_t value)
	{
		___feedbackGesture_11 = value;
	}
};

struct PlaytestSettings_t594028523_StaticFields
{
public:
	// Playtest.PlaytestSettings Playtest.PlaytestSettings::_current
	PlaytestSettings_t594028523 * ____current_2;

public:
	inline static int32_t get_offset_of__current_2() { return static_cast<int32_t>(offsetof(PlaytestSettings_t594028523_StaticFields, ____current_2)); }
	inline PlaytestSettings_t594028523 * get__current_2() const { return ____current_2; }
	inline PlaytestSettings_t594028523 ** get_address_of__current_2() { return &____current_2; }
	inline void set__current_2(PlaytestSettings_t594028523 * value)
	{
		____current_2 = value;
		Il2CppCodeGenWriteBarrier(&____current_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
