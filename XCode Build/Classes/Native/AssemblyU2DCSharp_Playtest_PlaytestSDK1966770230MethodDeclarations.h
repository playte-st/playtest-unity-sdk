﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t2474804324;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_Playtest_PlaytestSettings_Feedba2008949566.h"

// System.Void Playtest.PlaytestSDK::initializeSDK()
extern "C"  void PlaytestSDK_initializeSDK_m2163149047 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Playtest.PlaytestSDK::get_IsEnabled()
extern "C"  bool PlaytestSDK_get_IsEnabled_m679325735 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Playtest.PlaytestSDK::_setupFeedbackAndAppId(System.String,System.String)
extern "C"  void PlaytestSDK__setupFeedbackAndAppId_m1012030785 (Il2CppObject * __this /* static, unused */, String_t* ___feedbackId0, String_t* ___apiKey1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Playtest.PlaytestSDK::_setNeedsValidation(System.Boolean)
extern "C"  void PlaytestSDK__setNeedsValidation_m2477860215 (Il2CppObject * __this /* static, unused */, bool ___needsValidaton0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Playtest.PlaytestSDK::_setFeedbackMethod(System.Int32)
extern "C"  void PlaytestSDK__setFeedbackMethod_m1896283631 (Il2CppObject * __this /* static, unused */, int32_t ___feedbackMethod0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Playtest.PlaytestSDK::_setEngagementViewHidden(System.Boolean)
extern "C"  void PlaytestSDK__setEngagementViewHidden_m3390981853 (Il2CppObject * __this /* static, unused */, bool ___hidden0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Playtest.PlaytestSDK::_presentFeedbackScreen()
extern "C"  void PlaytestSDK__presentFeedbackScreen_m211580482 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Playtest.PlaytestSDK::_markCheckpoint(System.String,System.Boolean,System.String)
extern "C"  void PlaytestSDK__markCheckpoint_m382453560 (Il2CppObject * __this /* static, unused */, String_t* ___id0, bool ___withScreenshot1, String_t* ___eventData2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Playtest.PlaytestSDK::setupFeedbackId(System.String,System.String)
extern "C"  void PlaytestSDK_setupFeedbackId_m1420043508 (Il2CppObject * __this /* static, unused */, String_t* ___feedbackId0, String_t* ___apiKey1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Playtest.PlaytestSDK::setNeedsValidation(System.Boolean)
extern "C"  void PlaytestSDK_setNeedsValidation_m4058317696 (Il2CppObject * __this /* static, unused */, bool ___needsValidation0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Playtest.PlaytestSDK::setFeedbackMethod(Playtest.PlaytestSettings/FeedbackGestureType)
extern "C"  void PlaytestSDK_setFeedbackMethod_m697689533 (Il2CppObject * __this /* static, unused */, int32_t ___gestureType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Playtest.PlaytestSDK::SetEngagementViewVisible(System.Boolean)
extern "C"  void PlaytestSDK_SetEngagementViewVisible_m2551168888 (Il2CppObject * __this /* static, unused */, bool ___visible0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Playtest.PlaytestSDK::PresentFeedbackScreen()
extern "C"  void PlaytestSDK_PresentFeedbackScreen_m601086905 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Playtest.PlaytestSDK::MarkCheckpoint(System.String,System.Boolean,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void PlaytestSDK_MarkCheckpoint_m375979168 (Il2CppObject * __this /* static, unused */, String_t* ___id0, bool ___withScreenshot1, Dictionary_2_t2474804324 * ___args2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
