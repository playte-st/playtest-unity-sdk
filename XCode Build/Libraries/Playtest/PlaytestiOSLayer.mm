#include "UnityAppController.h"
#import "PTConnect.h"

// Converts C style string to NSString
NSString* CreateNSString (const char* string)
{
    if (string)
        return [NSString stringWithUTF8String: string];
    else
        return [NSString stringWithUTF8String: ""];
}

// Helper method to create C string copy
char* MakeStringCopy (const char* string)
{
    if (string == NULL)
        return NULL;
    
    char* res = (char*)malloc(strlen(string) + 1);
    strcpy(res, string);
    return res;
}

extern "C" {
    static UnityAppController* mainController = nil;
    
    void _setupFeedbackAndAppId(const char* feedbackId, const char* appId)
    {
        [[PTConnect manager] setRootViewController: UnityGetGLViewController()];
        
        [[PTConnect manager] setupFeedbackID:CreateNSString(feedbackId)
                                  withApiKey:CreateNSString(appId)];
        [[PTConnect manager] setContenttype: PTContentTypeGame];
        
    }
    void _setNeedsValidation(bool needsValidaton)
    {
        [[PTConnect manager] setNeedsValidation:needsValidaton];
    }
    void _setFeedbackMethod(int feedbackMethod)
    {
        [[PTConnect manager] setFeedbackmethod:feedbackMethod];
    }
    void _setEngagementViewHidden(bool hidden)
    {
        [[PTConnect manager] setEngagementViewHidden:hidden];
    }
    void _presentFeedbackScreen()
    {
        [[PTConnect manager] presentFeedbackScreen: UnityGetGLViewController()];
    }
    void _markCheckpoint(const char* checkpoint, bool withScreenshot, const char* args)
    {
        [[PTConnect manager] markCheckpoint:CreateNSString(checkpoint) withScreenshot:withScreenshot withEventString:CreateNSString(args) onCompletion:^(NSData * _Nullable responseData, NSError * _Nullable error) {
        }];
    }
}