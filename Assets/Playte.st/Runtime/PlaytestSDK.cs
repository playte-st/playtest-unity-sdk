﻿using UnityEngine;

namespace Playtest
{
    public static partial class PlaytestSDK
    {
        [RuntimeInitializeOnLoadMethod]
        public static void initializeSDK()
        {
            if (IsEnabled && Application.isPlaying)
            {            
                setupFeedbackId(PlaytestSettings.Current.FeedbackId, PlaytestSettings.Current.ApiKey);
                setNeedsValidation(PlaytestSettings.Current.NeedsValidaton);
                
                if(PlaytestSettings.Current.FeedbackGesture != PlaytestSettings.FeedbackGestureType.Disabled)
                    setFeedbackMethod(PlaytestSettings.Current.FeedbackGesture);
                
                if(!PlaytestSettings.Current.InitialMessageCenterVisibility)
                    SetEngagementViewVisible(false);
            }
        }
        
        public static bool IsEnabled
        {
            get
            {
#if UNITY_IPHONE || UNITY_ANDROID
                return PlaytestSettings.Current.Enabled; 
#else
                return false;
#endif
            }
        }
    }
}