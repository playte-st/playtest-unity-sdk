﻿#define ENABLE_LOGGING

#if UNITY_EDITOR && false
using System.Collections.Generic;
using UnityEngine;

namespace Playtest
{
    public static partial class PlaytestSDK
    {
        private static void setupFeedbackId(string feedbackId, string apiKey)
        {
            log("Initialising feedbackID: {0}, apiKey: {1}", feedbackId, apiKey);
        }

        private static void setNeedsValidation(bool needsValidation) { }        
        private static void setFeedbackMethod(PlaytestSettings.FeedbackGestureType gestureType) { }
        
        /// <summary>
        /// Sets the engagement view visible.
        /// </summary>
        /// <param name="visible">If set to <c>true</c> visible.</param>
        public static void SetEngagementViewVisible(bool visible) { log((visible ? "Showing" : "Hiding") + " engagement view"); }
        /// <summary>
        /// Presents the feedback screen.
        /// </summary>
        public static void PresentFeedbackScreen() { log("Presenting feedback screen"); }
        /// <summary>
        /// Marks the checkpoint.
        /// </summary>
        /// <param name="id">Identifier.</param>
        /// <param name="withScreenshot">If set to <c>true</c> with screenshot.</param>
        /// <param name="data">Optional data to include</param>
        public static void MarkCheckpoint(string id, bool withScreenshot = true, Dictionary<string, System.Object> data = null) { log("Marking checkpoint id: {0}, withScreenShot: {1}, withEventData: {2}", id, withScreenshot, Playtest.TinyJson.JSONWriter.ToJson(data)); }

        private static void log(string format, params object[] args)
        {
#if ENABLE_LOGGING
            Debug.LogFormat("<b>PlaytestSDK</b>: " + format, args);
#endif
        }
    }
}
#endif