﻿#if UNITY_ANDROID
using UnityEngine;
using System.Collections.Generic;

namespace Playtest
{
    public static partial class PlaytestSDK
    {
        private static AndroidJavaObject javaSDK;
        
        private static void setupFeedbackId(string feedbackId, string apiKey)
        {        
            AndroidJavaClass unityPlayerType = new AndroidJavaClass("com.unity3d.player.UnityPlayer"); 
            AndroidJavaObject currentActivity = unityPlayerType.GetStatic<AndroidJavaObject>("currentActivity");
            
            javaSDK = new AndroidJavaObject("com.esites.playtest.sdk.Playtest.PlaytestBuilder", 
                currentActivity.Call<AndroidJavaObject>("getApplication"),
                apiKey,
                feedbackId
            );
            javaSDK.Call("build");
        }

        
        private static void setNeedsValidation(bool needsValidation) { }        
        private static void setFeedbackMethod(PlaytestSettings.FeedbackGestureType gestureType) { }
        
        /// <summary>
        
        /// Sets the engagement view visible.
        /// </summary>
        /// <param name="visible">If set to <c>true</c> visible.</param>
        public static void SetEngagementViewVisible(bool visible) { log((visible ? "Showing" : "Hiding") + " engagement view"); }
        /// <summary>
        /// Presents the feedback screen.
        /// </summary>
        public static void PresentFeedbackScreen() { log("Presenting feedback screen"); }
        /// <summary>
        /// Marks the checkpoint.
        /// </summary>
        /// <param name="id">Identifier.</param>
        /// <param name="withScreenshot">If set to <c>true</c> with screenshot.</param>
        /// <param name="data">Optional data to include</param>
        public static void MarkCheckpoint(string id, bool withScreenshot = true, Dictionary<string, System.Object> data = null) { log("Marking checkpoint id: {0}, withScreenShot: {1}, withEventData: {2}", id, withScreenshot, Playtest.TinyJson.JSONWriter.ToJson(data)); }

        private static void log(string format, params object[] args)
        {
            Debug.LogFormat("<b>PlaytestSDK</b>: " + format, args);
        }
    }
}
#endif