﻿#if !UNITY_EDITOR && UNITY_IPHONE

using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;
using System.Collections.Generic;

namespace Playtest
{
    public static partial class PlaytestSDK
    {
        [DllImport("__Internal")]
        private static extern void _setupFeedbackAndAppId(string feedbackId, string apiKey);
        [DllImport("__Internal")]
        private static extern void _setNeedsValidation(bool needsValidaton);
        [DllImport("__Internal")]
        private static extern void _setFeedbackMethod(int feedbackMethod);
        [DllImport("__Internal")]
        private static extern void _setEngagementViewHidden(bool hidden);
        [DllImport("__Internal")]
        private static extern void _presentFeedbackScreen();
        [DllImport("__Internal")]
        private static extern void _markCheckpoint(string id, bool withScreenshot, string eventData);
        
        private static void setupFeedbackId(string feedbackId, string apiKey) { _setupFeedbackAndAppId(feedbackId, apiKey); }         
        private static void setNeedsValidation(bool needsValidation) { _setNeedsValidation(needsValidation); }        
        private static void setFeedbackMethod(PlaytestSettings.FeedbackGestureType gestureType) { _setFeedbackMethod((int)gestureType); }
        
        public static void SetEngagementViewVisible(bool visible) { _setEngagementViewHidden(visible); }
        public static void PresentFeedbackScreen() { _presentFeedbackScreen(); }
        public static void MarkCheckpoint(string id, bool withScreenshot = true, Dictionary<string, System.Object> args = null) { _markCheckpoint(id, withScreenshot, Playtest.TinyJson.JSONWriter.ToJson(args)); }
    }
}

#endif