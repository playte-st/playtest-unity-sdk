﻿//On other platforms the settings arent used, we know this and dont want a warning!
#pragma warning disable 0414
using UnityEngine;
using System.Collections;

namespace Playtest
{
    public class PlaytestSettings : ScriptableObject
    {
#if UNITY_EDITOR
        [UnityEditor.MenuItem("Window/Playtest SDK Settings")]
        static void show()
        {
            UnityEditor.Selection.activeObject = Current;
        }
#endif
        public enum FeedbackGestureType : int { Shake = 0, ThreeFinger = 1, FiveFinger = 2, Disabled = 3 }    

        private static PlaytestSettings _current;
        public static PlaytestSettings Current
        {
            get
            {
                if (_current == null)
                    _current = Resources.Load("PlaytestSettings") as PlaytestSettings;
                return _current;
            }
        }

        //enabled, unique id, api key, needs validation, feedback gesture, initial message center popover visibility.
        [SerializeField]
        private bool android_enabled = false, ios_enabled = false;

        [SerializeField]
        [Tooltip("Determines if the message center is visible when the application starts.")]
        private bool initialMessageCenterVisibility = true;
        [SerializeField]
        [Tooltip("When this is set only registered testers are allowed to start the application.")]
        private bool needsValidation = false;
        
        [SerializeField]
        [Tooltip("Authentication information can be found on our website.")]
        private string android_feedbackId = "", android_apiKey = "",
                        ios_feedbackId = "", ios_apiKey = "";
        [SerializeField]
        [Tooltip("With what gesture the feedback screen can be opened.")]
        private FeedbackGestureType feedbackGesture = FeedbackGestureType.Disabled;
                        
        public bool Enabled
        {
            get
            {
#if UNITY_IPHONE
                return ios_enabled;
#else
                return android_enabled;
#endif
            }
        }
        public string FeedbackId
        {
            get
            {
#if UNITY_IPHONE
                return ios_feedbackId;
#else
                return android_feedbackId;
#endif
            }
        }
        public string ApiKey
        {
            get
            {
#if UNITY_IPHONE
                return ios_apiKey;
#else
                return android_apiKey;
#endif
            }
        }

        public FeedbackGestureType FeedbackGesture { get { return feedbackGesture; } }
        public bool InitialMessageCenterVisibility { get { return initialMessageCenterVisibility; } }
        public bool NeedsValidaton { get { return needsValidation; } }
    }
}