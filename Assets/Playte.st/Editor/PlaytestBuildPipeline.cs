﻿using System;
using UnityEditor.iOS.Xcode;
using UnityEditor;
using UnityEditor.Callbacks;
using System.IO;
using System.Diagnostics;

namespace Playtest
{
    public static class PlaytestBuildPipeline
    {
        public static string GetPlaytestRootFolder()
        {
            return "Assets/Playte.st/"; //If you move Playte.st around, please edit this path so we can find the proper assets.
        }
                
        [PostProcessBuild]
        static void onPostProcessBuild(BuildTarget target, string path)
        {
            if(target == BuildTarget.iOS)
                processiOSBuild(path);
            else if(target == BuildTarget.Android)
                processAndroidBuild(path);
        }
        
        //http://forum.unity3d.com/threads/unity-xcode-api.281305/
        
        private static void processiOSBuild(string path)
        {
            string projPath = path + "/Unity-iPhone.xcodeproj/project.pbxproj";
            
            PBXProject proj = new PBXProject();
            proj.ReadFromString(File.ReadAllText(projPath));

            string target = proj.TargetGuidByName("Unity-iPhone");

            unzip(GetPlaytestRootFolder() + "Editor/PlaytestiOSNativeAssets.zip", Path.Combine(path, "Libraries/Playtest"));
                        
            string[] files = { "Libraries/Playtest/libPTConnect.a", "Libraries/Playtest/PlaytestiOSLayer.mm", "Libraries/Playtest/PTConnect.h", "Libraries/Playtest/PTResources.bundle" };
            foreach (string file in files)
                proj.AddFileToBuild(target, proj.AddFile(file, file, PBXSourceTree.Source));
            
            string[] requiredFrameworks = { "CFNetwork.framework", "CoreText.framework", "UIKit.framework", "AdSupport.framework", "CoreMedia.framework", "AVFoundation.framework", "Webkit.framework" };
            foreach(string framework in requiredFrameworks)
                proj.AddFrameworkToProject(target, framework, false);
            // Set a custom link flag
            proj.AddBuildProperty(target, "OTHER_LDFLAGS", "-ObjC -all_load");
            proj.AddBuildProperty(target, "LIBRARY_SEARCH_PATHS", "\"$(SRCROOT)/Libraries/Playtest\"");

            File.WriteAllText(projPath, proj.WriteToString());
        }
        
        private static void processAndroidBuild(string path)
        {
        
        }
        
        private static void unzip(string sourceArchive, string dstPath)
        {
            if(!Directory.Exists(dstPath))
                Directory.CreateDirectory(dstPath);
            string args = string.Format ("-o \"{0}\" -d \"{1}\"", sourceArchive, dstPath);
            
            Process.Start("unzip", args).WaitForExit();            
        }
        
        private static void copyAndReplaceDirectory(string srcPath, string dstPath)
        {
            if (Directory.Exists(dstPath))
                Directory.Delete(dstPath);
            if (File.Exists(dstPath))
                File.Delete(dstPath);
    
            Directory.CreateDirectory(dstPath);
    
            foreach (var file in Directory.GetFiles(srcPath))
                File.Copy(file, Path.Combine(dstPath, Path.GetFileName(file)));
    
            foreach (var dir in Directory.GetDirectories(srcPath))
                copyAndReplaceDirectory(dir, Path.Combine(dstPath, Path.GetFileName(dir)));
        }
    }
}