﻿using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System.Collections.Generic;

namespace Playtest
{
    [CustomEditor(typeof(PlaytestSettings))]
    public class PlaytestSettingsEditor : Editor
    {
        [DidReloadScripts]
        static void initializeSDK()
        {        
            //Ensure a PlayTestSettings exists.
            string[] foundSettings = AssetDatabase.FindAssets("t:PlaytestSettings");

            //No settings, create one.
            if (foundSettings.Length <= 0)
            {
                var inst = CreateInstance<PlaytestSettings>();

                AssetDatabase.CreateAsset(inst, PlaytestBuildPipeline.GetPlaytestRootFolder() + "Resources/PlaytestSettings.asset");
                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();
            }
        }

        private PlaytestSettings settings { get { return target as PlaytestSettings; } }
                
        public override void OnInspectorGUI()
        {
            SerializedProperty androidEnabledProp = serializedObject.FindProperty("android_enabled"),
                                iosEnabledProp = serializedObject.FindProperty("ios_enabled");
            bool androidEnabled = androidEnabledProp.boolValue,
                 iosEnabled = iosEnabledProp.boolValue;


            EditorGUILayout.HelpBox("You can find your Unique ID and API Key in our dashboard on http://www.playtest.st/.", MessageType.Info);
            androidEnabled = EditorGUILayout.BeginToggleGroup("Android", androidEnabled);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("android_feedbackId"), new GUIContent("Feedback ID"), true);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("android_apiKey"), new GUIContent("Api Key"), true);
            EditorGUILayout.EndToggleGroup();
            EditorGUILayout.Space();

            iosEnabled = EditorGUILayout.BeginToggleGroup("iOS", iosEnabled);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("ios_feedbackId"), new GUIContent("Feedback ID"), true);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("ios_apiKey"), new GUIContent("Api Key"), true);
            EditorGUILayout.EndToggleGroup();
            EditorGUILayout.Space();

            
            GUI.enabled = androidEnabled || iosEnabled;

            EditorGUILayout.LabelField("General settings", EditorStyles.boldLabel);

            foreach (string prop in new string[] { "feedbackGesture", "needsValidation", "initialMessageCenterVisibility" })
                EditorGUILayout.PropertyField(serializedObject.FindProperty(prop), true);

            androidEnabledProp.boolValue = androidEnabled;
            iosEnabledProp.boolValue = iosEnabled;
            serializedObject.ApplyModifiedProperties();

            GUI.enabled = true;
            string[] errors;
            EditorGUILayout.Space();
            if (hasErrors(androidEnabled, iosEnabled, out errors))
                EditorGUILayout.HelpBox("Please correct the following errors:\n - " + string.Join("\n - ", errors), MessageType.Error);
        }

        private bool hasErrors(bool isAndroid, bool isiOS, out string[] errors)
        {
            var errorList = new List<string>();

            if (!isAndroid && !isiOS)
            {
                errors = new string[] { "At least one platform has to be enabled!" };
                return true;
            }

            if (isAndroid)
            {
                if (string.IsNullOrEmpty(serializedObject.FindProperty("android_feedbackId").stringValue))
                    errorList.Add("Android unique ID must be filled in if the platform is enabled.");
                if (string.IsNullOrEmpty(serializedObject.FindProperty("android_apiKey").stringValue))
                    errorList.Add("Android api key must be filled in if the platform is enabled.");
            }

            if (isiOS)
            {
                if (string.IsNullOrEmpty(serializedObject.FindProperty("ios_feedbackId").stringValue))
                    errorList.Add("iOS unique ID must be filled in if the platform is enabled.");
                if (string.IsNullOrEmpty(serializedObject.FindProperty("ios_apiKey").stringValue))
                    errorList.Add("iOS api key must be filled in if the platform is enabled.");
            }
            errors = errorList.ToArray();
            return errorList.Count > 0;
        }
    }
}