﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class EpicGame : MonoBehaviour
{
    bool isVisible = true;

    void OnGUI()
    {
        GUI.matrix = Matrix4x4.TRS(Vector3.zero, Quaternion.identity, Vector3.one * 5f);
        GUI.enabled = Playtest.PlaytestSDK.IsEnabled;

        if (GUILayout.Button("SetEngagementViewVisible"))
            Playtest.PlaytestSDK.SetEngagementViewVisible(isVisible = !isVisible);
        if (GUILayout.Button("PresentFeedbackScreen"))
            Playtest.PlaytestSDK.PresentFeedbackScreen();
        if (GUILayout.Button("MarkCheckpoint"))
            Playtest.PlaytestSDK.MarkCheckpoint("checkpoint_1", true, new Dictionary<string, object>() { {"Time", Time.time}, {"Random", Random.value.ToString() } });
    }
}